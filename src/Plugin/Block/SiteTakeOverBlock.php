<?php

namespace Drupal\site_takeover\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilder;
use Drupal\media\Entity\Media;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Site Takeover Block.
 *
 * @Block(
 *   id          = "site_takeover",
 *   admin_label = @Translation("Site Takeover"),
 *   category    = @Translation("Site Takeover"),
 * )
 */
class SiteTakeOverBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Form\FormBuilder definition.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * SiteTakeoverBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Form\FormBuilder $form_builder
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    ModuleHandler $module_handler,
    EntityTypeManagerInterface $entity_type_manager,
    FormBuilder $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'background_image' => NULL,
        'background_image_style' => '',
        'background_responsive_image_style' => '',
        'use_active_theme_logo' => TRUE,
        'logo_image' => '',
        'title' => '',
        'body' => '',
        'show_button' => TRUE,
        'button_text' => 'Continue',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // Only show block when site takeover is enabled.
    $config = $this->configFactory->get('site_takeover.settings');
    $site_takeover_status = !empty($config->get('site_takeover_status')) && $config->get('site_takeover_status');
    if (!$site_takeover_status) {
      return AccessResult::forbidden();
    }
    // If on emergency page, hide block
    $site_takeover_page = $config->get('site_takeover_page');
    if (!empty($site_takeover_page)) {
      // Get current route/node we're on
      $node = \Drupal::routeMatch()->getParameter('node');
      if ($node instanceof \Drupal\node\NodeInterface) {
        // If they match, don't show block
        if ($node->id() == $site_takeover_page) {
          return AccessResult::forbidden();
        }
      }
    }
    // Always allow access otherwise
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['takeover'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Takeover settings'),
      '#open' => TRUE,
    ];

    $form['takeover']['background'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Background settings'),
    ];

    $background_image = NULL;
    if (!empty($config['background_image'])) {
      $background_image = Media::load($config['background_image']);
    }
    $form['takeover']['background']['background_image'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'media',
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['image'],
      ],
      '#title' => $this->t('Background Image'),
      '#default_value' => $background_image,
      '#required' => FALSE,
    ];

    $image_styles = ImageStyle::loadMultiple();
    $background_image_styles = [];
    foreach ($image_styles AS $id => $image_style) {
      $background_image_styles[$id] = $image_style->label();
    }
    $form['takeover']['background']['background_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Background image style'),
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select image style for background image.'),
      '#options' => $background_image_styles,
      '#default_value' => !empty($config['background_image_style']) ? $config['background_image_style'] : '',
      '#required' => FALSE,
    ];

    $responsive_image_styles = ResponsiveImageStyle::loadMultiple();
    $background_responsive_image_styles = [];
    foreach ($responsive_image_styles AS $id => $image_style) {
      $background_responsive_image_styles[$id] = $image_style->label();
    }
    $form['takeover']['background']['background_responsive_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Background responsive image style'),
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select responsive image style for background image.'),
      '#options' => $background_responsive_image_styles,
      '#default_value' => !empty($config['background_responsive_image_style']) ? $config['background_responsive_image_style'] : '',
      '#required' => FALSE,
    ];

    $form['takeover']['logo'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logo settings'),
    ];

    $form['takeover']['logo']['use_active_theme_logo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the logo supplied by active theme'),
      '#description' => $this->t('Pull and display logo supplied by active theme.'),
      '#default_value' => ($config['use_active_theme_logo']) ? $config['use_active_theme_logo'] : FALSE,
    ];

    $logo_image = NULL;
    if (!empty($config['logo_image'])) {
      $logo_image = Media::load($config['logo_image']);
    }
    $form['takeover']['logo']['logo_image'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'media',
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['image'],
      ],
      '#title' => $this->t('Logo Image'),
      '#default_value' => $logo_image,
      '#required' => FALSE,
    ];

    $form['takeover']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => !empty($config['title']) ? $config['title'] : '',
      '#required' => FALSE,
    ];

    $form['takeover']['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => !empty($config['body']['value']) ? $config['body']['value'] : '',
      '#required' => FALSE,
    ];

    $form['takeover']['show_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show/hide button'),
      '#description' => $this->t('Show or hide redirect button.'),
      '#default_value' => !empty($config['show_button']) ? $config['show_button'] : TRUE,
    ];

    $form['takeover']['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#default_value' => !empty($config['button_text']) ? $config['button_text'] : 'Continue',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['title'] = $values['takeover']['title'];
    $this->configuration['body'] = $values['takeover']['body'];

    $this->configuration['background_image'] = $values['takeover']['background']['background_image'];
    $this->configuration['background_image_style'] = $values['takeover']['background']['background_image_style'];
    $this->configuration['background_responsive_image_style'] = $values['takeover']['background']['background_responsive_image_style'];

    $this->configuration['use_active_theme_logo'] = $values['takeover']['logo']['use_active_theme_logo'];
    $this->configuration['logo_image'] = $values['takeover']['logo']['logo_image'];

    $this->configuration['show_button'] = $values['takeover']['show_button'];
    $this->configuration['button_text'] = $values['takeover']['button_text'];
  }

  /**
   * Render responsive image.
   *
   * @param \Drupal\media\Entity\Media $image
   * @param string $image_style
   *
   * @return mixed
   */
  private function renderResponsiveImage($image, $image_style) {

    $build = NULL;

    // Set display options.
    $display_options = [
      'label' => 'hidden',
      'type' => 'responsive_image',
      'settings' => [
        'responsive_image_style' => $image_style,
      ],
    ];

    // Get image, apply display options.
    if (!$image->get('field_media_image')->isEmpty()) {
      $build = $image->get('field_media_image')->view($display_options);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $background_image = NULL;
    $background_image_src = NULL;
    if (!empty($this->configuration['background_image'])) {
      $background_image_entity = Media::load($this->configuration['background_image']);
      if ($background_image_entity && $background_image_entity->hasField('field_media_image')) {
        $background_image_uri = $background_image_entity->get('field_media_image')->entity->getFileUri();
        $background_image_src = ImageStyle::load($this->configuration['background_image_style'])
          ->buildUrl($background_image_uri);
        if (!empty($this->configuration['background_responsive_image_style'])) {
          $background_image = $this->renderResponsiveImage($background_image_entity, $this->configuration['background_responsive_image_style']);
        }
      }
    }

    $logo_image = NULL;
    if (!empty($this->configuration['logo_image']) && !$this->configuration['use_active_theme_logo']) {
      $logo_image_entity = Media::load($this->configuration['logo_image']);
      if ($logo_image_entity && $logo_image_entity->hasField('field_media_image')) {
        $logo_image = file_create_url($logo_image_entity->get('field_media_image')->entity->getFileUri());
      }
    }

    if ($this->configuration['use_active_theme_logo']) {
      $logo_image = theme_get_setting('logo.url');
    }

    $site_config = $this->configFactory->get('system.site');
    $site_takeover_config = $this->configFactory->get('site_takeover.settings');

    $build = [
      '#theme' => 'site_takeover',
      '#logo' => [
        'use_site_logo' => $this->configuration['use_active_theme_logo'],
        'logo_image' => $logo_image,
        'site_name' => $site_config->get('name'),
        'site_slogan' => $site_config->get('slogan'),
      ],
      '#title' => $this->configuration['title'],
      '#body' => $this->configuration['body'],
      '#button' => [
        'show_button' => $this->configuration['show_button'],
        'button_text' => $this->configuration['button_text'],
        'page_id' => $site_takeover_config->get('site_takeover_page'),
      ],
      '#background' => [
        'responsive_image' => $background_image,
        'src' => $background_image_src,
      ],
    ];

    $build['#attached']['library'][] = 'site_takeover/site_takeover';

    $cacheableMetadata = new CacheableMetadata();
    $cacheableMetadata->addCacheableDependency($this->configuration);
    $cacheableMetadata->applyTo($build);

    return $build;
  }
}
