<?php

/**
 * @file
 * Contains
 *   \Drupal\site_takeover\EventSubscriber\SiteTakeoverRequestSubscriber
 */

namespace Drupal\site_takeover\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SiteTakeoverRequestSubscriber
 *
 * @package Drupal\site_takeover\EventSubscriber
 */
class SiteTakeoverRequestSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $pageCacheKillSwitch;

  /**
   * Valid routes that should be skipped or not redirected on.
   *
   * @var array
   */
  protected static $validRoutes = [
    'user.login',
    'user.logout',
    'user.reset',
    'user.reset.form',
    'user.reset.login',
    'user.pass',
    'user.page',
    'entity.user.canonical',
    'site_takeover.takeover_redirect',
  ];

  /**
   * SiteTakeoverRequestSubscriber constructor.
   *
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $page_cache_kill_switch
   */
  public function __construct(
    UrlGeneratorInterface  $url_generator,
    PathMatcherInterface   $path_matcher,
    ConfigFactoryInterface $config,
    AdminContext           $admin_context,
    ModuleHandler          $module_handler,
    KillSwitch             $page_cache_kill_switch
  ) {
    $this->urlGenerator = $url_generator;
    $this->pathMatcher = $path_matcher;
    $this->config = $config->get('site_takeover.settings');
    $this->adminContext = $admin_context;
    $this->moduleHandler = $module_handler;
    $this->pageCacheKillSwitch = $page_cache_kill_switch;
  }

  /**
   * Site takeover handler.
   *
   * @param RequestEvent $event
   */
  public function SiteTakeover(RequestEvent $event) {

    // Get request
    $request = $event->getRequest();

    // Get current route
    $current_route = \Drupal::routeMatch()->getRouteName();

    // Get config items
    $site_takeover_status = !empty($this->config->get('site_takeover_status')) ? $this->config->get('site_takeover_status') : FALSE;
    $site_takeover_redirect_type = $this->config->get('site_takeover_redirect_type');
    $site_takeover_redirect_page = $this->config->get('site_takeover_page');
    $site_takeover_redirect_code = $this->config->get('site_takeover_redirect_code');

    // If not master request, stop here.
    // Site takeover not enabled, stop here.
    if (!$event->isMasterRequest() || !$site_takeover_status) {
      return;
    }

    // Redirect type set to 'none', stop here.
    if ($site_takeover_redirect_type == 'none') {
      return;
    }

    // Check redirect type
    if ($site_takeover_redirect_type == 'site-wide' || ($site_takeover_redirect_type == 'front' && $this->pathMatcher->isFrontPage())) {

      // Internal page caching messes with event subscribers!
      if ($this->moduleHandler->moduleExists('page_cache')) {
        $this->pageCacheKillSwitch->trigger();
      }

      // If cookie set, bypass takeover.
      if ($request->cookies->has('site-takeover')) {
        return;
      }

      // If valid route, don't perform redirect.
      if (in_array($current_route, self::$validRoutes)) {
        return;
      }

      // Check if admin route, don't perform redirect.
      if ($this->adminContext->isAdminRoute()) {
        return;
      }

      // Get redirect details from config settings.
      $redirect_url = '';
      if (!empty($site_takeover_redirect_page)) {
        $redirect_url = Url::fromRoute('entity.node.canonical', ['node' => $site_takeover_redirect_page], [])
          ->toString();
      }

      // Current page is takeover page. Stop redirect flow/process here.
      if (!empty($redirect_url) && $request->getRequestUri() == $redirect_url) {
        return;
      }

      // Perform redirect
      if (!empty($redirect_url) && $site_takeover_redirect_code) {

        // Append back the request query string from $_SERVER.
        $query_string = $request->server->get('QUERY_STRING');
        if ($query_string) {
          $redirect_url .= '?' . $query_string;
        }

        $response = new TrustedRedirectResponse($redirect_url, $site_takeover_redirect_code);
        $response->getCacheableMetadata()
          ->addCacheContexts(['cookies:site-takeover']);
        $event->setResponse($response);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['SiteTakeover', 31];
    return $events;
  }

}
