<?php
/**
 * @file
 * Contains Drupal\site_takeover\Controller\SiteTakeoverController.
 */

namespace Drupal\site_takeover\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class SiteTakeoverController
 *
 * @package Drupal\site_takeover\Controller
 */
class SiteTakeoverController extends ControllerBase {

  /**
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * SiteTakeoverController constructor.
   *
   * @param \Drupal\Core\Render\Renderer $renderer
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Site takeover redirect route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function takeoverRedirect(Request $request) {
    $response = new RedirectResponse(Url::fromRoute('<front>', [])
      ->toString());
    if (!$request->cookies->has('site-takeover')) {
      $cookie = new Cookie('site-takeover', '1', 0, '/', NULL, FALSE);
      $response->headers->setCookie($cookie);
    }
    return $response;
  }
}
