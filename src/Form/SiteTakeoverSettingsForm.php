<?php

namespace Drupal\site_takeover\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class SiteTakeoverSettingsForm
 *
 * @package Drupal\site_takeover\Form
 */
class SiteTakeoverSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_takeover.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_takeover_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_takeover.settings');

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    $form['settings']['site_takeover_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Takeover status'),
      '#description' => $this->t('Enable or disable site emergency takeover. If enabled, emergency takeover block will be accessible.'),
      '#default_value' => $config->get('site_takeover_status'),
    ];

    $site_takeover_page = NULL;
    if (!empty($config->get('site_takeover_page'))) {
      $site_takeover_page = Node::load($config->get('site_takeover_page'));
    }
    $form['settings']['site_takeover_page'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Takeover page'),
      '#description' => $this->t('Select page to be used as emergency page. This page will essentially display an overview of the situation, for your users.'),
      '#default_value' => $site_takeover_page,
      '#selection_settings' => [
        'target_bundles' => ['page',],
      ],
    ];

    $redirect_types = [
      'none' => $this->t('No redirect'),
      'site-wide' => $this->t('Site wide'),
      'front' => $this->t('Front page only'),
    ];
    $form['settings']['site_takeover_redirect_type'] = [
      '#type' => 'select',
      '#options' => $redirect_types,
      '#description' => $this->t('Determine when the redirect should occur. Either site wide or only on homepage. Currently defaults to site wide.'),
      '#empty_option' => $this->t('Select redirect type'),
      '#default_value' => !empty($config->get('site_takeover_redirect_type')) ? $config->get('site_takeover_redirect_type') : 'site-wide',
    ];

    $redirect_status_codes = [
      301 => $this->t('301 - Moved Permanently'),
      302 => $this->t('302 - Found (Previously "Moved temporarily")'),
      303 => $this->t('303 - See Other'),
      304 => $this->t('304 - Not Modified'),
      305 => $this->t('305 - Use Proxy'),
      306 => $this->t('306 - Switch Proxy'),
      307 => $this->t('307 - Temporary Redirect'),
      308 => $this->t('308 - Permanent Redirect'),
    ];
    $form['settings']['site_takeover_redirect_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Redirect status code'),
      '#options' => $redirect_status_codes,
      '#empty_option' => $this->t('Select status code'),
      '#default_value' => !empty($config->get('site_takeover_redirect_code')) ? $config->get('site_takeover_redirect_code') : 302,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Settings.
    $this->config('site_takeover.settings')
      ->set('site_takeover_status', $form_state->getValue('site_takeover_status'))
      ->set('site_takeover_page', $form_state->getValue('site_takeover_page'))
      ->set('site_takeover_redirect_type', $form_state->getValue('site_takeover_redirect_type'))
      ->set('site_takeover_redirect_code', $form_state->getValue('site_takeover_redirect_code'))
      ->save();
  }

}
